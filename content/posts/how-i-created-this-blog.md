+++
title = "How I Created this Blog"
date = "2016-10-10T11:27:10-04:00"
categories = ["hugo", "gitlab"]
+++

I have been toying with the idea to use [`hugo`][1] as my static web site generator for few weeks now. And then the news of its version 0.17 release are [announced][4], and `hugo` shows up on [Hacker News once again][3].

<!--more-->

Somehow while browsing through the comments on HN, I end up on [Gitlab Pages][2], and there I read:

> While you can create a project from scratch, let's keep it simple and fork one of your favorite example projects to get a quick start. GitLab Pages works with **any** *(emphasis mine)* static site generator.

From there I navigate to the [Example projects for various static site generators][5] and Hugo is one of them!

![Gitlab Pages Examples - Hugo](/images/gitlab_pages_examples_hugo.png)

So then I proceed to the [`hugo` example][6], and then proceed through the following steps to get my own *Gitlab Page* running!

1. *Fork* the [`hugo` example][6]
2. In the fork's *Settings (Gear icon) > Runners*, I ensure that shared runners are enabled. These runners are what rebuild my static web page each time I update the content/site configuration.
3. In *Settings > Edit Project*,
   - Navigate to **Rename repository** section and change the fields in **Project name** and **Path** to `NAMESPACE.gitlab.io` where *NAMESPACE* is your gitlab *username*.
   - Navigate further down in the same page and remove the forking relationship (optional).
4. In `config.toml`, change this `baseurl` line, from `"https://pages.gitlab.io/hugo/"` to `"https://NAMESPACE.gitlab.io"` where *NAMESPACE* is your gitlab *username*. Then **commit and push those changes**.

Within few minutes when the shared runner finished building the site, my site was available on [`https://kaushalmodi.gitlab.io`][8].

The source repo for this site is [here][7].

## Port from Octopress

After setting up Gitlab and `hugo` CI, it was then pretty straightforward to port in my *very old* posts from my Octopress blog.

## Tweaking the Gitlab CI configuration script

I tweaked the Gitlab provided `.gitlab-ci.yml` to achieve two things:

1. Use the latest `hugo` release as of today (version 0.17)
2. Use [*Pygments*][10] for syntax highlighting.

So below was the result ([Source][9]):

```yaml
image: alpine:3.4

before_script:
  - apk update && apk add openssl
  # Install Pygments for syntax highlighting
  # https://gitlab.com/gitlab-com/support-forum/issues/1150#note_16763556
  - apk add py-pygments
  - wget https://github.com/spf13/hugo/releases/download/v0.17/hugo_0.17_Linux-64bit.tar.gz
  - echo "6a74626b64434a5ee9fc5ec4fbf22ce6  hugo_0.17_Linux-64bit.tar.gz" | md5sum -c
  - tar xf hugo_0.17_Linux-64bit.tar.gz && cp ./hugo_0.17_linux_amd64/hugo_0.17_linux_amd64 /usr/bin/hugo
  - hugo version

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

In order to make `hugo` use *Pygments*, you need to put the below in your `config.toml`:

```
pygmentsStyle = "trac" # This is just the Pygments theme I like
pygmentsCodeFences = true
```

[1]: https://gohugo.io/
[2]: https://pages.gitlab.io/
[3]: https://news.ycombinator.com/item?id=12672394
[4]: http://spf13.com/post/hugo-goes-global/
[5]: https://gitlab.com/groups/pages
[6]: https://gitlab.com/pages/hugo
[7]: https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io
[8]: https://kaushalmodi.gitlab.io
[9]: https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io/blob/master/.gitlab-ci.yml
[10]: http://pygments.org/
