+++
title = "Formatting in LaTeX"
date = "2014-05-29T10:48:39-04:00"
categories = ["latex", "formatting"]
+++

Notes and references for formatting in LaTeX.

<!--more-->

* LaTeX formatting examples - [Getting to Grips with LaTeX](http://www.andy-roberts.net/writing/latex/formatting)
* Replace the default LaTeX font to a crisper font using the code snippet suggested in [Tips on Writing a Thesis in LaTeX](http://www.khirevich.com/latex/font/)
```latex
\usepackage[T1]{fontenc}
\usepackage{charter}
\usepackage[expert]{mathdesign}
```

* Using Inconsolata font to format the code blocks using `minted` package - [tex.stackexchange.com](http://tex.stackexchange.com/questions/85932/mintedinconsolata-straight-quotes)
