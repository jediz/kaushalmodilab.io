+++
title = "Escaping dollar signs in tcsh"
date = "2014-03-06T16:12:56-05:00"
categories = ["tcsh", "find", "escaping", "regex"]
+++

I found how to escape a $ sign
in a regex expression in a tcsh alias. BUT it is UGLY!

<!--more-->

I wanted to set an alias for a `find` command containing the `-regex`
switch. For simplicity I will use this example:
```sh
find . -type f -regex '.*\.txt$'
```

This expression simply gives a list of all *.txt files in any
directory under the current path.

The above command works fine when running in the terminal, but when
saving that to a `tcsh` alias, that `$` needs to be escaped:
```sh
alias findtxt "find . -type f -regex '.*txt'\"\$"''"
```

> A simple `$` has to be written as `'\"\$"'`!!!

Granted that I will usually get the same result if I did `alias
findtxt "find . -type f -regex '.*txt'"` instead. But this turned out
to be an interesting exercise on how to escape a `$`.

[Reference][1]

[1]: http://stackoverflow.com/questions/3571743/csh-alias-with-perl-one-liner-evaluates-when-alias-is-created-and-not-when-alias
