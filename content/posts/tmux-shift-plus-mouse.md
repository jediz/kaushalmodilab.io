+++
title = "tmux Shift+Mouse"
slug = "tmux-shift-plus-mouse"
date = "2014-08-28T16:47:46-04:00"
categories = ["tmux", "mouse"]
+++

In tmux, I had been missing the select and middle-click method for copying and
pasting stuff in terminal using mouse.

<!--more-->

Thanks to [this][2] post, I figured that I can use the `Shift` key and bypass
the tmux copy and paste method.

* Shift + Mouse left button double-click -- Select word and copy it
* Shift + Select using mouse -- Copy the selection
* Shift + Mouse middle button click -- Paste the text copied using above method
  in the tmux terminal

I am using xterm, tcsh 6.18.01 and tmux 1.9a

[My tmux config][1]

[1]: https://github.com/kaushalmodi/dotfiles/blob/master/dot_tmux.conf
[2]: http://superuser.com/questions/598718/how-do-i-select-entire-words-with-tmuxs-mouse-mode
