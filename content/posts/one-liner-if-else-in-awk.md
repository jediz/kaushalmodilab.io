+++
title = "One liner if-else in awk"
date = "2014-03-18T16:00:53-04:00"
categories = ["awk", "condition", "oneliner"]
+++

Tiny snippets showing if/else use in `awk` ..

<!--more-->

```sh
echo abc:def | awk -F: '{ if ( $2 ) {print $2} else {print} }'
```
Prints `def`.

```sh
echo abc | awk -F: '{ if ( $2 ) {print $2} else {print} }'
```
Prints `abc`.
