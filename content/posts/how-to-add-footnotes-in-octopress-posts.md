+++
title = "How to add footnotes in Octopress posts?"
date = "2014-03-06T10:47:35-05:00"
categories = ["octopress", "markdown", "footnotes"]
+++

**Update (2017/02/23)** --- Now I use Hugo instead of Octopress for
blogging. But the same method of using `[^1]` works for creating
footnotes.

---

While looking for a way to add footnotes to my posts, I came across
this [post][s1].

Based on that, here is how I got the footnotes working:

<!--more-->

- To create a footnote, use the standard MultiMarkdown `[^1]` anchor
  to create the footnote reference link, and add `[^1]: The footnote
  content` to the bottom of the file. Footnote example[^1].
- To make the footnotes smaller, lighter and closer together, add the
  following CSS to `sass/custom/_styles.css` file:
    ```css
    .footnotes {
      font-size: 13px;
      line-height: 16px;
      color: #666;

      p {
        margin-bottom: 6px;
      }
    }
    ```

[s1]: http://hiltmon.com/blog/2013/05/08/octopress-now-has-footnotes/

[^1]: I am a little footnote. [Hyperlinks](http://kaushalmodi.github.io) are also allowed here. Note that the index style `[Page Title][linkindex]` links don't work in the footnotes; the links have to be defined using the `[Page Title](link)` syntax.
