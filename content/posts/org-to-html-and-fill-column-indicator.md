+++
title = "Org-to-HTML and fill-column-indicator"
date = "2014-07-21T13:06:09-04:00"
categories = ["emacs", "org", "html"]
+++

Lately I was having an issue in the org to html conversion where the newline
characters got appended with funky unicode characters.

Full detail is in [this][s1] reddit post I started.

<!--more-->

The fix was to add the following code after the line `(funcall lang-mode)` in
the `org-html-fontify-code` defun in `ox-html.el`.

```elisp
(when (require 'fill-column-indicator nil 'noerror)
    (fci-mode -1))
```

*Make sure you delete ox-html.elc else your patched ox-html.el won't be effective.*

[s1]: http://www.reddit.com/r/emacs/comments/2b5x5g/funny_characters_appended_at_new_line_to_source/
