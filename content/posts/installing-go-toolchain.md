+++
date = "2017-02-24T01:33:47-05:00"
title = "Installing go toolchain"
slug = "installing-go-toolchain"
categories = ["go"]
+++

There are 2 reasons why I suggest installing `go` to anymore, whether
they are Go developers, or not (like me).

1. You can then build amazing utilities like [peco][1], [hugo][2] and
   [noti][3].
2. **It's easy!**

<!--more-->

## Installing `go`

Below instructions are for installing `go` on a 64-bit GNU/Linux
machine, and using `tcsh` shell. But similar steps can be applied for
any other OS and shell.

1. Download the *tar.gz* for the latest *linux-amd64* binaries from
   <https://golang.org/dl/>.
2. Extract it to some place in your `$HOME`. *I extracted it to
   `${HOME}/go1.8/`*.
3. Create a directory where you would want to install the `go` packages:
    ```tcsh
    mkdir -p ~/go.apps
    ```

4. Set the following environment variables[^fn:1], and also save them
    to your shell config:
    ```tcsh
    setenv GOROOT ${HOME}/go1.8 # go root
    setenv GOPATH ${HOME}/go.apps # for go applications
    ```

5.  Add the `${GOROOT}/bin` and `${GOPATH}/bin` directories to your
    `$PATH`.

Now you can install any `go` application!

---

For instance, `noti` is a nice little utility that triggers an
alert (desktop popup, Pushbullet notification, etc.) when a process
finishes. From its [installation notes][4], you just run the below to
install it:
```
go get -u github.com/variadico/noti/cmd/noti
```

Apart from the `go` applications I suggested here, *go* out and
explore more -- `go get` them :)

[^fn:1]: You can refer to these official `go` references [[1][5],[2][6]] for further information on these variables.

[1]: https://github.com/peco/peco
[2]: https://github.com/spf13/hugo
[3]: https://github.com/variadico/noti
[4]: https://github.com/variadico/noti#installation
[5]: https://golang.org/doc/install#tarball_non_standard
[6]: https://golang.org/doc/install#testing
