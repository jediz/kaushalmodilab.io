+++
title = "Generating a transparent favicon"
date = "2016-10-15T01:27:35-04:00"
categories = ["Web Design"]
tags = ["favicon", "transparent", "browser"]
+++

While generating a simple favicon for this blog, I went through a little exercise on how to generate a favicon with transparent background. I did not want the favicon with square block background which runs a risk of not blending well with the browser tab background as seen below:

![Bad Favicon][4]

<!--more-->

## Inkscape and transparent png

Here are the steps I took to make this icon transparent. I used [Inkscape][1] and [Dynamic Drive's online favicon generator][2] to make this happen.

1. Inkscape sets the background of the image to be transparent by default. But here is how to verify that that's the case. Do <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>d</kbd> or *Files > Document Properties*, click on the *Background color* and ensure that the *Alpha* component of *RGBA Hex* is `00`.
 ![Inkscape - Transparent Background][5]
2. This next step when saving the image is crucial. Do not simply *Save* or *Save As* the image. That will *not* save the transparent properties in the image!

Thanks to [this *graphicdesign.stackexchange* solution][3],

> Just using *save as -> png* does not apply the transparent background. However, the transparent background is the default when you export: *File > Export Bitmap -> Export*.

In newer Inkscape versions (at least in version 0.91), that option is *File > Export PNG Image* (or <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>e</kbd>) instead. Note that when you do that, instead of a pop-up window, you will see an attached window (shown below) appear on the side.

![Inkscape - Export PNG Image][6]

## `.png` or `.ico`

Another question I faced when working on this was whether I should save the favicon as a `.png` or .`ico` because I have seen both of these types being used.

Thanks to [this *stackoverflow* solution][7],

> All modern browsers (tested with Chrome 4, Firefox 3.5, IE8, Opera 10 and Safari 4) will always request a favicon.ico unless you've specified a shortcut icon via \<link\>.

The answer also says that `.ico` files support transparency just fine and that's where I learned about the [online favicon generator tool][2].

## Summary

- Created a transparent `.png` in Inkscape.
- Converted it to `.ico` using [that][2] online tool.
- And the result!
 ![Good Favicon][8]

---

## Update - Oct 20 2016

Today I was browsing this site on my Android phone and I noticed that the Google Chrome browser did not show this favicon on the list of recently visited websites.

That bugged me and a little bit of googling led me to an even better online favicon generator website: [realfavicongenerator.net][9].

I simply needed to upload my Inkscape created `.png` (with transparent background) to that site, and it provided a favicon package for different platforms and an HTML code to be put in the `<head>`:

```html
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#000000">
```

The website also shows previews of how the icon would look on different platforms: regular browser tabs, iOS, Android, etc.

Here is how this favicon looks on Android Chrome after updating the icons and this website's `<head>`.

![Favicon in Android Chrome][10]

[1]: https://inkscape.org/en/
[2]: http://tools.dynamicdrive.com/favicon/
[3]: http://graphicdesign.stackexchange.com/a/44411/78149
[4]: /images/favicon_not_good.png
[5]: /images/favicon_transparent_background.png
[6]: /images/favicon_export_png_image.png
[7]: http://stackoverflow.com/a/1344379/1219634
[8]: /images/favicon_good.png
[9]: http://realfavicongenerator.net/
[10]: /images/favicon_android_chrome.png
