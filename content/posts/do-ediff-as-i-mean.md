+++
title = "Do ediff as I mean"
date = "2015-03-09T17:31:45-04:00"
categories = ["emacs", "elisp", "ediff"]
+++

In almost all of my `ediff` use cases, I would have windows open side-by-side
in a frame, and then I would want to do a diff between the two using
`ediff-buffers`.

But emacs doesn't know that. So it always asks me to select the buffers I want
to diff. The same problem is with `ediff-files` too.

So I came up with the below helper function to pick the correct `ediff` command ..

<!--more-->

```elisp
(defun modi/ediff-dwim ()
        "Do ediff as I mean.

If a region is active when this command is called, call `ediff-regions-wordwise'.

Else if the current frame has 2 windows,
- Do `ediff-files' if the buffers are associated to files and the buffers
  have not been modified.
- Do `ediff-buffers' otherwise.

Otherwise call `ediff-buffers' interactively."
        (interactive)
        (if (region-active-p)
            (call-interactively 'ediff-regions-wordwise)
          (if (= 2 (safe-length (window-list)))
              (let (bufa bufb filea fileb)
                (setq bufa  (get-buffer (buffer-name)))
                (setq filea (buffer-file-name bufa))
                (save-excursion
                  (other-window 1)
                  (setq bufb (get-buffer (buffer-name))))
                (setq fileb (buffer-file-name bufb))
                (if (or
                     ;; if either of the buffers is not associated to a file
                     (null filea) (null fileb)
                     ;; if either of the buffers is modified
                     (buffer-modified-p bufa) (buffer-modified-p bufb))
                    (progn
                      (message "Running (ediff-buffers \"%s\" \"%s\") .." bufa bufb)
                      (ediff-buffers bufa bufb))
                  (progn
                    (message "Running (ediff-files \"%s\" \"%s\") .." filea fileb)
                    (ediff-files filea fileb))))
            (call-interactively 'ediff-buffers))))
```

[Find it on my git][1].

[1]: https://github.com/search?utf8=%E2%9C%93&q=user%3Akaushalmodi+extension%3Ael++%22defun+modi%2Fediff-dwim%22&type=Code
