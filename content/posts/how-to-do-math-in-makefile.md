+++
title = "How to do math in Makefile?"
date = "2014-03-05T13:38:08-05:00"
categories = ["makefile", "bc"]
+++

If you have `bc` installed, you can use it to do math operations on
variables in a *Makefile*.

<!--more-->

For me, it was installed in `/usr/bin/bc`.

```makefile
VAR_A = 10
VAR_B = 11
VAR_C = $(shell echo $(VAR_A)\*$(VAR_B) | bc)

default:
	echo $(VAR_C)
```
