+++
date = "2017-03-07T21:36:13-05:00"
title = "Installing bleeding edge Hugo + Goorgeous"
slug = "installing-bleeding-edge-hugo-goorgeous"
categories = ["hugo", "org-mode"]

+++

You already got `go` installed, and that's why are you reading it.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If not -- Go! [Install it!][1]

So.. now that you have `go` installed, why settle for the
release version of [`hugo`][2]? You are reading this blog post
generated using the bleeding edge of `hugo`[^fn:1].

In addition, Hugo v0.19 onwards supports the [*org-mode*][4]
syntax[^fn:2], which is so much more awesome than *markdown*,
especially if you are using emacs.

This post is about how you can install the latest versions of `hugo` and the go
package that adds the *org-mode* support -- [`goorgeous`][3].

<!--more-->

## First time install of hugo

When any package gets installed using `go get`, it gets installed
inside the `$GOPATH/src/` directory. We install `hugo` using `go get`
for this *one-and-only* time to get that correct directory structure
in there:

```sh
go get -u -v github.com/spf13/hugo
```

## Installing hugo from master

Now you should find the `hugo` source code in
`$GOPATH/src/github.com/spf13/hugo/`.

**From here on, run all commands in that directory!&nbsp;**[^fn:3]

To get the latest files from the *master* branch do:
```sh
git fetch --all # fetch new branch names if any
git checkout master
# I also do hard reset as I don't develop any code in go
git reset --hard origin/master
```

### Govendor

Hugo does its package dependency management using [`govendor`][5],
which, not surprisingly, is also a `go` package. (`goorgeous` is one
of these dependent packages.)

So you would install `govendor` like any other `go` package:

```sh
go get -u -v github.com/kardianos/govendor
```

The package dependency database is stored in
`$GOPATH/src/github.com/spf13/hugo/vendor/vendor.json`. This JSON
file specifies what other `go` packages need to be installed from
which git repo, using which commit.

Govendor makes installing the right versions of the dependent packages
easy -- Just run the below.

```sh
govendor sync
```

### Latest `goorgeous`

The commit hash for `goorgeous` in that JSON file might not
point to its latest version. But we are interested in getting the
latest-and-greatest *org-mode* support for `hugo` ..

The good news is that `govendor` allows that:

```sh
# Fetch the goorgeous package from its master branch
govendor fetch github.com/chaseadamsio/goorgeous
```

*See the Govendor [Quick Start Guide][6] for frequently used
commands.*

### Static Executable

We want to make the `hugo` executable a **static** executable.

That way it is *100% portable* --- independent of dynamically linked
libraries. This is also how the *Continuous Integration* engines (like
the Gitlab CI Runner that generates this blog) can use this custom
built `hugo` binary without any dynamic library dependency issue.

To enable static building of `go` binaries, these env variables need
to be set:

```bash
export CGO_ENABLED=0
export GO_EXTLINK_ENABLED=0
```

*Note that above (and the snippet that follows) work in a `bash`
shell. Even if your default shell is not `bash`, you can run the [full
`bash` script](#full-build-script) at the end of this post.*

### Building Hugo

We now finally build `hugo`, with some customization so that
running `hugo version` prints the *Build Date* and *Commit Hash*
too.

```sh
package="github.com/spf13/hugo"
commithash=$(git rev-parse --short HEAD 2>/dev/null)
builddate=$(date +%FT%T%z)
go install -v \
   -ldflags "-X ${package}/hugolib.CommitHash=${commithash} \
             -X ${package}/hugolib.BuildDate=${builddate}" \
   ${package}
```

This will install the binary as `$GOPATH/bin/hugo`.

*Now don't forget to add `$GOPATH/bin` to your `$PATH`.*

Verify that the binary got built as expected by running `hugo
version`.

> Hugo Static Site Generator v0.20-DEV-A2EC372A linux/amd64 BuildDate: 2017-03-07T23:38:45-05:00

## Full Build Script

Here is the full `bash` script. You can save it as `hugo-build.sh` and
it will run just fine on any shell (as long as you haven't removed
`bash` from your system :smile:).

```bash
#!/usr/bin/env bash

here=$(pwd)

package="github.com/spf13/hugo"

export CGO_ENABLED=0
export GO_EXTLINK_ENABLED=0

if ! hash govendor 2>/dev/null
then
    go get -u -v github.com/kardianos/govendor
fi

# Install hugo for the first time so that the ${GOPATH}/src/${package}
# directory gets populated.
if ! hash hugo 2>/dev/null
then
    go get -u -v ${package}
fi

# Update to hugo master branch
cd ${GOPATH}/src/${package}
git fetch --all # fetch new branch names if any
git checkout master
# Force update the vendor file in case it got changed
git reset --hard origin/master

# Synchronize all the dependent packages as per the just updated vendor file
govendor sync

# Update the goorgeous package to its master branch
govendor fetch github.com/chaseadamsio/goorgeous

commithash=$(git rev-parse --short HEAD 2>/dev/null)
builddate=$(date +%FT%T%z)
go install -v \
   -ldflags "-X ${package}/hugolib.CommitHash=${commithash} \
             -X ${package}/hugolib.BuildDate=${builddate}" \
   ${package}

echo "Hugo Version Check:"
hugo version

cd ${here}
```

[^fn:1]: To see the master branch commit of `hugo` used to build this site, do <kbd>Ctrl</kbd> + <kbd>U</kbd> in your browser and search for *Hugo Commit Hash*.
[^fn:2]: Hugo v0.19 [Release Notes](https://gohugo.io/meta/release-notes/#0-19-february-27th-2017)
[^fn:3]: You need to be in the `${GOPATH}/src/${package}` directory in order to build any `go` `${package}` (unless you are doing a plain `go get`).

[1]: {{< relref "installing-go-toolchain.md" >}}
[2]: https://gohugo.io/
[3]: https://github.com/chaseadamsio/goorgeous
[4]: http://orgmode.org/
[5]: https://github.com/kardianos/govendor
[6]: https://github.com/kardianos/govendor#quick-start-also-see-the-faq
