+++
title = "Installing custom fonts in Linux"
date = "2014-03-27T13:42:19-04:00"
categories = ["font", "linux", "xterm", "tcsh"]
+++

I'll step through how to set custom fonts for xterm terminal. My default shell is `tcsh`.

<!--more-->

1. Create a folder `.fonts` in your $HOME directory.
2. Download fonts of your choice (**ttf** or **otf** format to ~/.fonts).
3. Refresh the fonts cache with `fc-cache -fv`.
4. You can verify if your custom fonts got added to the cache using `fc-list`. For example, I would do `fc-list -f "%{family}\n" | sort -u | grep 'Inconsolata'`[^1] to check if my downloaded Inconsolata fonts got into the font cache.
5. Add the below .Xdefaults snippet to your `~/.Xdefaults`
6. Add `xrdb -merge $HOME/.Xdefaults` to your shell init script. *My shell init script is `~/.alias`*.

```text
*customization: -color
XTerm*termName: xterm-256color

xterm*saveLines:        500
xterm*scrollBar:        false
xterm*cursorColor:      white
xterm*pointerColor:     white
xterm*Foreground:       white
xterm*Background:       black
xterm*c132:             true
xterm*loginShell:       false

! Fonts
! XTerm*faceName: DejaVu Sans Mono:size=11
! XTerm*faceName: Inconsolata:size=11
! XTerm*faceName: Inconsolata\\-dz:style=dz:size=11
XTerm*faceName: Inconsolata\\-g:style=g:size=11
```

Done! Now source your shell init script and launch `xterm`.

**Note**: In order to use font names with hyphens in them, I had to escape them by using `\\`. So for the `Inconsolata-g` font, I have `XTerm*faceName: Inconsolata\\-g:style=g:size=11`.

It might be helpful to add the below aliases to your tcsh init script for quick font refresh and check, using `fontsrefresh; fontsavail | grep 'Inconsolata'`.

```sh
alias fontsavail   'fc-list -f "%{family}\n" | sort -u'
alias fontsrefresh 'fc-cache -fv'
```

[^1]: You can download Inconsolata font from [here](http://levien.com/type/myfonts/inconsolata.html).
