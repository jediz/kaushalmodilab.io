+++
date = "2017-01-12T12:07:43-05:00"
publishdate = "2017-01-14T00:00:00-05:00"
title = "Stuff about command-line ftp"
slug = "stuff-about-command-line-ftp"
categories = ["ftp"]

+++

To connect to a machine with ip *MACHINE*, you simply need to enter the below at command-line:
```bash
ftp MACHINE
```

See below for a list of commands you would typically use once you `ftp` to a machine.

<!--more-->

## Basic FTP Commands
<!--- BEGIN RECEIVE ORGTBL ftp-commands -->
| Command | Description |
|---|---|
| `?` or `help` | Get help on FTP commands |
| `bye` or `quit` | Exit the FTP session |
| `binary` | Set the mode of file transfer to *binary* |
|  | *(Provides less chance of transmission error)* |
| `cd`, `ls`, `mkdir`, `pwd`, `rmdir` | Unix operations on remote machine |
| `delete` | Unix `rm` operation on remote machine |
| `lcd` | Unix `cd` operation on local machine |
| `put` *FILE* | Copy 1 file from local to remote |
| `mput` *FILES* (ex: `mput *`) | Copy multiple files from local to remote |
| `get` *FILE* | Copy 1 file from remote to local |
| `mget` *FILES* (ex: `mget*`) | Copy multiple files from remote to local |
<!--- END RECEIVE ORGTBL ftp-commands -->
<!---
#+ORGTBL: SEND ftp-commands orgtbl-to-gfm
| Command                             | Description                                    |
|-------------------------------------+------------------------------------------------|
| `?` or `help`                       | Get help on FTP commands                       |
| `bye` or `quit`                     | Exit the FTP session                           |
| `binary`                            | Set the mode of file transfer to *binary*      |
|                                     | *(Provides less chance of transmission error)* |
| `cd`, `ls`, `mkdir`, `pwd`, `rmdir` | Unix operations on remote machine              |
| `delete`                            | Unix `rm` operation on remote machine          |
| `lcd`                               | Unix `cd` operation on local machine           |
| `put` *FILE*                        | Copy 1 file from local to remote               |
| `mput` *FILES* (ex: `mput *`)       | Copy multiple files from local to remote       |
| `get` *FILE*                        | Copy 1 file from remote to local               |
| `mget` *FILES* (ex: `mget*`)        | Copy multiple files from remote to local       |
-->

## Automating FTP logins for specific IPs
If you find yourself logging into a specific *MACHINE* too frequently, and if you want to save yourself from having to enter the username and password each time, you can save those credentials to your `~/.netrc` file. See the example at the end of this post.

If that file does not exist, then create it, and **remember to set its permission to 600**.
```bash
touch ~/.netrc
chmod 600 ~/.netrc
```

If the permissions are not set so, you will get this ftp error:
```
Error - .netrc file not correct mode.
Remove password or correct mode.
```

### Ignore the credentials in `.netrc`
But at times, you might need to manually log into a machine with a different set of credentials than the one saved in the `~/.netrc`. To do so, run `ftp` with the `-n` option. And then use the `quote USER` and `quote PASS` commands to pass in the username and password.
```
> ftp -n <machine>
ftp> quote USER <username>
ftp> quote PASS <password>
```

### Commenting out stuff in `.netrc`
Unfortunately, `.netrc` does not seem to have a comment syntax. But this hack works ..

Using the `#` character quickly followed by an FTP command *foo* (no space, ex: `#foo`) is one way to "comment out" lines with valid .netrc keywords.

> You cannot have such 'comments' after valid .netrc keywords are used.

So if you need to comment out a set of logins and passwords, do that **before** the first valid set of uncommented login credentials. See below for an example of that.

## An Example `~/.netrc`
```
#machine foo.bar.com
#login username1
#password pAsSw0rd1

machine foo.bar.com
login username2
password pAsSw0rd2
```

## References

- [Basic FTP Commands][1] - Colorado State Univerity
- [Use configuration file for ftp with auto login enabled upon initial connection][2] - *unix.stackexchange*
- `man ftp`, `man netrc`

[1]: https://www.cs.colostate.edu/helpdocs/ftp.html
[2]: http://unix.stackexchange.com/a/28440/57923
