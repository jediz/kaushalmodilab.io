#!/usr/bin/env bash
# Script to check a website recursively for dead links (images too).
# http://www.createdbypete.com/articles/simple-way-to-find-broken-links-with-wget/
# https://discuss.gohugo.io/t/link-checker-for-go-hugo/2202

site="http://scripter.co"
log="/tmp/${USER}_wget_dead_link_check.log"
wget --spider -o ${log} -e robots=off -w 1 -r -p ${site}
# --spider, this tells Wget not to download anything since we only want a report
#           so it will only do a HEAD request not a GET.
# -o ${log}, log messages to the declared file
# -e robots=off, this one tells wget to ignore the robots.txt file. Learn more
#                about robots.txt (http://www.robotstxt.org/).
# -w 1, adds a 1 second wait between requests, this slows down Wget to more
#       consistent rate to minimise stress on the hosting server so you don’t
#       get back any false positives.
# -r, this means recursive so Wget will keep trying to follow links deeper into
#     your sites until it can find no more!
# -p, get all page requisites such as images, etc. needed to display HTML page
#     so we can find broken image links too.
# ${site}, finally the website url to start from.
\grep --color -B 2 '404 Not Found' ${log}
